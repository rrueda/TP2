package com.fisi.tp2.sse.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fisi.tp2.sse.model.entity.Egresado;
import com.fisi.tp2.sse.service.UsuarioService;

@Controller
public class InicioController {
	
	@Autowired
	UsuarioService usuarioService;
	
	@RequestMapping(value="/login", method = RequestMethod.POST)
	public String login(HttpServletRequest request, @RequestParam String codigo, @RequestParam String contrasena) throws Exception {
		if(request.getSession().getAttribute("mensajeError") != null){
			request.getSession().removeAttribute("mensajeError");
			return "login";
		}
		
		Egresado egresado = usuarioService.obtenerEgresadoLogin(codigo, contrasena);
		if(egresado != null){
			request.getSession().setAttribute("usuarioSesion", egresado);
			return "home";
		}else{
			request.getSession().setAttribute("mensajeError", "C�digo y/o contrase�a incorrecta");
			return "login";
		}	
	}
	
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request) throws Exception{	
		HttpSession session = request.getSession();
		session.invalidate();	
		return "login";	
	}
}
