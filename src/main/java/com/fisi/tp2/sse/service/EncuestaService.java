package com.fisi.tp2.sse.service;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisi.tp2.sse.model.bean.PreguntaBean;
import com.fisi.tp2.sse.model.entity.Egresado;
import com.fisi.tp2.sse.model.entity.Encuesta;

@Transactional(propagation = Propagation.REQUIRED)
public interface EncuestaService {
	
	public String obtenerPreguntasEncuesta(int idEncuesta) throws Exception;
	public Integer guardarCuestionarioResuelto(List<PreguntaBean> preguntas, Egresado egresado) throws Exception;
	public String obtenerReporte1() throws Exception;
	public String obtenerReporteCausaNoTitulo() throws Exception;
	public String obtenerReporteMaximoGrado() throws Exception;
	public String obtenerReporteEstimacionExpectativaEscuela() throws Exception;
	public String obtenerReporteRecomiendaFormacionEcuela() throws Exception;	
	public String obtenerReporteNivelRelacionEmpleo() throws Exception;
	public String obtenerReporteNivelSatisfaccionTrabajo() throws Exception;
	public String obtenerReporteNivelRespuestaExigenciaMercado() throws Exception;
	public String guardarEncuesta(Encuesta encuesta) throws Exception;
	public int obtenerCantidadEncuestas() throws Exception;
}
