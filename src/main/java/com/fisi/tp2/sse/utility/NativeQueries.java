package com.fisi.tp2.sse.utility;

public class NativeQueries {
	
	public static final String reporte1 = "SELECT re.descripcion as titulo, COUNT(re.idRespuesta) as cantidad FROM egresadopregunta ep "
			+ "INNER JOIN respuesta re ON re.idRespuesta = ep.idRespuestaFk "
			+ "INNER JOIN pregunta pr ON pr.idPregunta = re.idPreguntaFk "
			+ "WHERE idPregunta = 1 "
			+ "GROUP BY re.idRespuesta";
	public static final String reporteCausaNoTitulo = "SELECT re.descripcion as causa, COUNT(re.idRespuesta) as cantidad FROM egresadopregunta ep "
			+ "INNER JOIN respuesta re ON re.idRespuesta = ep.idRespuestaFk "
			+ "INNER JOIN pregunta pr ON pr.idPregunta = re.idPreguntaFk "
			+ "WHERE idPregunta = 3 "
			+ "GROUP BY re.idRespuesta";
	public static final String reporteMaximoGrado = "SELECT re.descripcion as grado, COUNT(re.idRespuesta) as cantidad FROM egresadopregunta ep "
			+ "INNER JOIN respuesta re ON re.idRespuesta = ep.idRespuestaFk "
			+ "INNER JOIN pregunta pr ON pr.idPregunta = re.idPreguntaFk "
			+ "WHERE idPregunta = 4 "
			+ "GROUP BY re.idRespuesta";
	
	public static final String reporteEstimacionExpectativaEscuela = "SELECT re.descripcion as expectativa, COUNT(re.idRespuesta) as cantidad FROM egresadopregunta ep "
			+ "INNER JOIN respuesta re ON re.idRespuesta = ep.idRespuestaFk "
			+ "INNER JOIN pregunta pr ON pr.idPregunta = re.idPreguntaFk "
			+ "WHERE idPregunta = 5 "
			+ "GROUP BY re.idRespuesta";
	
	public static final String reporteRecomiendaFormacionEcuela = "SELECT re.descripcion as recomienda, COUNT(re.idRespuesta) as cantidad FROM egresadopregunta ep "
			+ "INNER JOIN respuesta re ON re.idRespuesta = ep.idRespuestaFk "
			+ "INNER JOIN pregunta pr ON pr.idPregunta = re.idPreguntaFk "
			+ "WHERE idPregunta = 6 "
			+ "GROUP BY re.idRespuesta";
	
	public static final String reporteNivelRelacionEmpleo = "SELECT re.descripcion as porcentaje, COUNT(re.idRespuesta) as cantidad FROM egresadopregunta ep "
			+ "INNER JOIN respuesta re ON re.idRespuesta = ep.idRespuestaFk "
			+ "INNER JOIN pregunta pr ON pr.idPregunta = re.idPreguntaFk "
			+ "WHERE idPregunta = 13 "
			+ "GROUP BY re.idRespuesta";
	
	public static final String reporteNivelSatisfaccionTrabajo = "SELECT re.descripcion as porcentaje, COUNT(re.idRespuesta) as cantidad FROM egresadopregunta ep "
			+ "INNER JOIN respuesta re ON re.idRespuesta = ep.idRespuestaFk "
			+ "INNER JOIN pregunta pr ON pr.idPregunta = re.idPreguntaFk "
			+ "WHERE idPregunta = 17 "
			+ "GROUP BY re.idRespuesta";
	
	public static final String reporteNivelRespuestaExigenciaMercado = "SELECT re.descripcion as porcentaje, COUNT(re.idRespuesta) as cantidad FROM egresadopregunta ep "
			+ "INNER JOIN respuesta re ON re.idRespuesta = ep.idRespuestaFk "
			+ "INNER JOIN pregunta pr ON pr.idPregunta = re.idPreguntaFk "
			+ "WHERE idPregunta = 17 "
			+ "GROUP BY re.idRespuesta";
	
	public static final String obtenerCantidadEncuestas = "SELECT count(*) FROM Encuesta";
}
