package com.fisi.tp2.sse.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fisi.tp2.sse.model.bean.MensajeRespuesta;
import com.fisi.tp2.sse.model.bean.PreguntaBean;
import com.fisi.tp2.sse.model.bean.UsuarioBean;
import com.fisi.tp2.sse.model.entity.Egresado;
import com.fisi.tp2.sse.service.UsuarioService;

@Controller
public class UsuarioController {
	
	@Autowired
	UsuarioService usuarioService;
	
	@RequestMapping(value="/obtenerUsuarioPorPk", method = RequestMethod.POST)
	public @ResponseBody String obtenerUsuarioPorPk(HttpServletRequest request, @RequestBody UsuarioBean usuario) throws Exception{		
		String jsonResponse = null;
//		jsonResponse = usuarioService.obtenerUsuarioPorPk(usuario.getIntUsuarioId());			
		return jsonResponse; 
	}
	
	@RequestMapping(value="/actualizarDatosEgresado", method = RequestMethod.PUT)
	public @ResponseBody MensajeRespuesta actualizarDatosEgresado(HttpServletRequest request, @RequestBody UsuarioBean usuarioBean) throws Exception{		
		
		Egresado egresado = (Egresado) request.getSession().getAttribute("usuarioSesion");
		
		String[] nombre = usuarioBean.getNombre().split(" ");
		
		egresado.setApellidos(nombre[0] + " " + nombre[1]);
		egresado.setNombres(nombre[2] + " " + nombre[3]);
		egresado.setDireccion(usuarioBean.getDireccion());
		
		usuarioService.actualizarEgresadoLogin(egresado);
		
		MensajeRespuesta mensaje = new MensajeRespuesta();
		mensaje.setMessage("Se ha actualizado tus datos personales de acuerdo a la Reniec");		
		return mensaje; 
	}
	
}
