package com.fisi.tp2.sse.persistence.dao;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Blob;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.stream.JsonWriter;

@Repository
public class HibernateCrudDAO<T> implements ICrudDAO<T> {

  private SessionFactory sessionFactory;
  
  @Autowired
  public void setSessionFactory(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

	public T persist(T t) {
	  sessionFactory.getCurrentSession().saveOrUpdate(t);
	  return t;
	}

	public T persistList(List<T> t) throws Exception {
		
		try {
			for (Iterator<T> iter=t.iterator(); iter.hasNext();) {
				  sessionFactory.getCurrentSession().saveOrUpdate(iter.next());
			}
		} catch (HibernateException e) {
			throw e;
		}
	  return null;
	}

	@SuppressWarnings("unchecked")
	public T findById(Class<T> type, Object id) {
	  return (T) sessionFactory.getCurrentSession().createCriteria(type).add(Restrictions.idEq(id)).uniqueResult();
	}

	public void delete(T t) throws Exception {
	  sessionFactory.getCurrentSession().delete(t);
	}
		
	@SuppressWarnings("unchecked")
	public List<T> findByNamedQuery(String namedQuery,Map<String, Object> parameters) {
	  Query query = sessionFactory.getCurrentSession().getNamedQuery(namedQuery);
	  if(parameters != null){
	  	  for (String key : parameters.keySet()) {
	  		  if(parameters.get(key) instanceof List){
	  			  query.setParameterList(key, (List)parameters.get(key));
	  		  }else{
	  			  query.setParameter(key, parameters.get(key));	  
	  		  }
	  	  }
	  }
	  return query.list();
	}
	public T findObjectByNamedQuery(String namedQuery, Map<String, Object> parameters){
		  Query query = sessionFactory.getCurrentSession().getNamedQuery(namedQuery);
		  if(parameters != null){
		  	  for (String key : parameters.keySet()) {
		  		  if(parameters.get(key) instanceof List){
		  			  query.setParameterList(key, (List)parameters.get(key));
		  		  }else{
		  			  query.setParameter(key, parameters.get(key));	  
		  		  }
		  	  }
		  }
		  return (T)query.uniqueResult();
	}

	public List findByNamedQueryTransformer(Class<?> type, String namedQuery, Map<String, Object> parameters){
		  Query query = sessionFactory.getCurrentSession().getNamedQuery(namedQuery);
		  if(parameters != null){
		  	  for (String key : parameters.keySet()) {
		  		  if(parameters.get(key) instanceof List){
		  			  query.setParameterList(key, (List)parameters.get(key));
		  		  }else{
		  			  query.setParameter(key, parameters.get(key));	  
		  		  }
		  	  }
		  }
		  
		  query.setResultTransformer(new AliasToBeanResultTransformer(type));		  
		  return query.list();		
	}
	
	public List findByNativeQueryTransformer(Class<?> type, String nativeQuery, Map<String, Object> parameters){
		
		StringBuffer sb = new StringBuffer(nativeQuery);		
		
		SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sb.toString());
		
		if(parameters != null){
			for (String key : parameters.keySet()) {
				if(parameters.get(key) instanceof List){
					query.setParameterList(key, (List)parameters.get(key));
				}else{
					query.setParameter(key, parameters.get(key));	  
				}
			}
		}
		  
		query.setResultTransformer(new AliasToBeanResultTransformer(type));		  
		return query.list();		
	}
	
	public Object findObjectByNamedQueryTransformer(Class<?> type, String namedQuery, Map<String, Object> parameters){
	  Query query = sessionFactory.getCurrentSession().getNamedQuery(namedQuery);
	  if(parameters != null){
	  	  for (String key : parameters.keySet()) {
	  		  if(parameters.get(key) instanceof List){
	  			  query.setParameterList(key, (List)parameters.get(key));
	  		  }else{
	  			  query.setParameter(key, parameters.get(key));	  
	  		  }
	  	  }
	  }
	  
	  query.setResultTransformer(new AliasToBeanResultTransformer(type));		  
	  return query.uniqueResult();		
	}
	public String findByNamedQueryJson(String namedQuery, Map<String, Object> parameters){
		  Query query = sessionFactory.getCurrentSession().getNamedQuery(namedQuery);
		  if(parameters != null){
		  	  for (String key : parameters.keySet()) {
		  		  if(parameters.get(key) instanceof List){
		  			  query.setParameterList(key, (List)parameters.get(key));
		  		  }else{
		  			  query.setParameter(key, parameters.get(key));	  
		  		  }
		  	  }
		  }

		  query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		  List<Map<String,Object>> aliasToValueMapList=query.list();
		  return convertMapListToJson(aliasToValueMapList) ;
	}

	public int executeUpdateNamedQuery(String namedQuery,Map<String, Object> parameters){
		  Query query = sessionFactory.getCurrentSession().getNamedQuery(namedQuery);
		  if(parameters != null){
		  	  for (String key : parameters.keySet()) {
		  		  if(parameters.get(key) instanceof List){
		  			  query.setParameterList(key, (List)parameters.get(key));
		  		  }else{
		  			  query.setParameter(key, parameters.get(key));	  
		  		  }
		  	  }
		  }
		  
//		  query.setResultTransformer(arg0)
		  
		  return query.executeUpdate();
	}

	
	
	@SuppressWarnings("unchecked")
	public List<Object[]> execQuery(String stQuery) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery(stQuery);
	    return (List<Object[]> )query.list();
	}	
	
	@SuppressWarnings("unchecked")
	public Blob createBlob(MultipartFile multipartFile) {
		Blob blob =null;
		try {
			blob = Hibernate.getLobCreator(sessionFactory.getCurrentSession()).createBlob(multipartFile.getInputStream(),multipartFile.getSize());
			
		} catch (Exception e) {
			
		}
		return blob;
	}
	
	@SuppressWarnings("unchecked")
	public Blob createBlob(InputStream inputStream,long size) {
		Blob blob =null;
		try {
			blob = Hibernate.getLobCreator(sessionFactory.getCurrentSession()).createBlob(inputStream,size);
			
		} catch (HibernateException e) {
			
		}
		return blob;
	}	
	
	@SuppressWarnings("unchecked")
	public Blob createBlob(byte [] array) {
		Blob blob =null;
		try {
			blob = Hibernate.getLobCreator(sessionFactory.getCurrentSession()).createBlob(array);
			
		} catch (HibernateException e) {
			
		}
		return blob;
	}		
	
	public List<String> getAuthorities(Integer userId) {
		
		  StringBuffer sb = new StringBuffer("");
		  sb.append("					select et.ET_ST_SHORT_NAME as 'authority' "); 
		  sb.append("					from rol_user_account rua "); 
		  sb.append("					inner join rol ro on (ro.RO_ID_ROL_PK = rua.RO_ID_ROL_FK) "); 
		  sb.append("					inner join rol_option_permission rop on (rop.RO_ID_ROL_FK = rua.RO_ID_ROL_FK) "); 
		  sb.append("					inner join element_table et on (et.ET_ID_ELEM_TAB_PK = rop.ROP_ID_PERMISSION_FK) "); 
		  sb.append("					where rua.UA_ID_USER_ACCOUNT_FK = :id "); 
		  sb.append("					AND et.ET_IN_STATUS = 1 and ro.RO_IN_STATUS = 1 "); 
		  sb.append("					group by et.ET_ST_SHORT_NAME ");

		    
		    SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sb.toString());
		    
		    query.setParameter("id", userId);
		    
		    return query.list();
	}
	
	public Object[] getResumeAttemptsFailedLogin(Integer userId, Date fromDate,Integer failedAttemptsNumberSystem) {

		StringBuffer sb = new StringBuffer("");

		
		sb.append("select count(1), max(AUL_ID_ATTEMPTS_LOGIN_PK), sum(AUL_IN_FAILED) from ( "); 
		sb.append("select AUL_ID_ATTEMPTS_LOGIN_PK, AUL_IN_FAILED "); 
		sb.append("from ATTEMPTS_USER_LOGIN "); 
		sb.append("WHERE UA_ID_USER_ACCOUNT_FK = :userId "); 
		sb.append("and  AUL_DT_LAST_MODIFIED >=  :fromDate "); 
		sb.append("order by AUL_ID_ATTEMPTS_LOGIN_PK desc "); 
		sb.append("limit :failedAttemptsNumberSystem ) tmp ");
		
		SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(
				sb.toString());

		query.setParameter("userId", userId);
		query.setParameter("fromDate", fromDate);
		query.setParameter("failedAttemptsNumberSystem",failedAttemptsNumberSystem);

		return (Object[]) query.list().get(0);
	}
	
	public String executeNativeQueryJson(String strQuery, Map<String, Object> parameters) {		
		StringBuffer sb = new StringBuffer(strQuery);		
		
		SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sb.toString());
		
		if(parameters != null){
			for (String key : parameters.keySet()) {
			  if(parameters.get(key) instanceof List){
				  query.setParameterList(key, (List)parameters.get(key));
			  }else{
				  query.setParameter(key, parameters.get(key));	  
			  }
			}
		}	
		
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String,Object>> aliasToValueMapList = query.list();
		return convertMapListToJson(aliasToValueMapList);
	}		
	
	public String convertMapListToJson(List<Map<String,Object>> aliasToValueMapList){
		
		ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(outputStream);        
		JsonWriter writer;
		try {
			writer = new JsonWriter(new OutputStreamWriter(out));
			
	        writer.beginArray();
			
			for (Map<String,Object> map: aliasToValueMapList){
				  writer.beginObject();
				  for (Map.Entry<String,Object> entry : map.entrySet()) {
					    String key = entry.getKey();
					    Object thing = entry.getValue();
					    writer.name(key); 
					    if(thing!=null && !thing.toString().trim().equals("")){
					    	if(thing instanceof Timestamp){
					    		Date objDate = (Date)thing;
						    	writer.value(convertDatetoString(objDate, "HH:mm:ss dd/MM/yyyy"));
					    	}else{
						    	writer.value(thing.toString().replace("'", "\'"));
					    	}    		
					    }else{
					    	writer.value("");
					    }
					    
				  }
				  writer.endObject();
			}			
			
			writer.endArray();
			writer.close();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return outputStream.toString();
	}
	
	public String convertDatetoString(Date dtDate, String strFormat) {
		String strNewDate = "";
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(strFormat);
			if (dtDate != null)
				strNewDate = sdf.format(dtDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strNewDate;
	}
}
