package com.fisi.tp2.sse.model.bean;

public class RespuestaBean{
	
	private Integer respuestaId;
	private String descripcion;
	
	public Integer getRespuestaId() {
		return respuestaId;
	}
	public void setRespuestaId(Integer respuestaId) {
		this.respuestaId = respuestaId;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}