'use strict';

app.controller('homeCtrl', function($scope, $http, $state, $uibModal, $q) {

	var $ctrl = this;
	$ctrl.idEncuesta = angular.element('#idEncuesta').val();
	$ctrl.idPersona = angular.element('#idPersona').val();
	$ctrl.animationsEnabled = true;
	$ctrl.open = function(size, parentSelector) {
		var parentElem = parentSelector ? angular.element($document[0]
				.querySelector('.modal-demo ' + parentSelector)) : undefined;
		var modalInstance = $uibModal.open({
			animation : $ctrl.animationsEnabled,
			ariaLabelledBy : 'modal-title',
			ariaDescribedBy : 'modal-body',
			templateUrl : 'myModalContent.html',
			controller : 'ModalInstanceCtrl',
			controllerAs : '$ctrl',
			size : size,
			appendTo : parentElem,
			resolve : {
				items : function() {
					var delay = $q.defer();
					$http.get('obtenerPreguntasEncuesta', {params: {idEncuesta: $ctrl.idEncuesta}}).success(
							function(data, status, headers, config) {
								$ctrl.data = data;
								delay.resolve(data);
							}).error(function(data, status, headers, config) {
						$state.go("Usted ha respondido a todos los cuestionarios");
					});
					return delay.promise;
				}
			}
		});
	};
	
	$ctrl.open();
	
	
	
});

app.controller('ModalInstanceCtrl', function($http, $uibModalInstance, items) {
	
	if(items == "" ){
		$uibModal.dismiss('cancel');
	}
	
	var $ctrl = this;
	$ctrl.items = items;
	
	$ctrl.selected = {
		item : $ctrl.items[0]
	};

	$ctrl.ok = function() {
		$http.post('guardarCuestionarioResuelto', $ctrl.items).success(
				function(data) {
					swal({
						title : 'Enviado',
						type : 'success',
						text : data.message,
						timer : 3000,
						showConfirmButton : false
					});
					$uibModalInstance.close($ctrl.selected.item);
				}).error(function(data) {
			console.log("error al guardar cuestionario");
		});
	};

	$ctrl.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	};
	
});
