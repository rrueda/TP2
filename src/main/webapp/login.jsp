<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head> 
    	<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="stylesheet" href="app/styles/font-awesome.min.css">
		<link rel="stylesheet" href='app/styles/fonts.css'>
		<link rel="stylesheet" href="app/plugin/bootstrap/bootstrap.min.css">
		<link rel="stylesheet" href="app/styles/style.css">

		<title>Login</title>
	</head>
	<body>
		<div class="container">
			<div class="row main">
				<div class="panel-heading">
	               <div class="panel-title text-center">
	               		<h1 class="title">Sistema de seguimiento de egresados</h1>
	               		<hr />
	               	</div>
	            </div> 
				<div class="main-login main-center">
					<form class="form-horizontal" method="post" action="login">
						<div class="form-group">
							<label for="username" class="cols-sm-2 control-label">Código</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="codigo" id="codigo"  placeholder="Ingresa tu código"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Contraseña</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="contrasena" id="contrasena"  placeholder="Ingresa tu contraseña"/>
								</div>
							</div>
						</div>

						<div class="form-group ">
							<button type="submit" class="btn btn-primary btn-lg btn-block login-button">Iniciar sesión</button>
						</div>
						<c:if test="${not empty mensajeError}">
							<div class="form-group ">
								<label style="color:red">${mensajeError}</label>
							</div>
						</c:if>												
					</form>
				</div>
			</div>
		</div>		
	</body>
</html>