package com.fisi.tp2.sse.utility;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.google.gson.stream.JsonWriter;


public class CommonUtilities {
	
	public static String convertDatetoString(Date dtDate, String strFormat) {
		String strNewDate = "";
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(strFormat);
			if (dtDate != null)
				strNewDate = sdf.format(dtDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strNewDate;
	}
	
	public static String convertMapListToJson(List<Map<String,Object>> aliasToValueMapList){
		
		ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(outputStream);        
		JsonWriter writer;
		try {
			writer = new JsonWriter(new OutputStreamWriter(out));
			
	        writer.beginArray();
			
			for (Map<String,Object> map: aliasToValueMapList){
				  writer.beginObject();
				  for (Map.Entry<String,Object> entry : map.entrySet()) {
					    String key = entry.getKey();
					    Object thing = entry.getValue();
					    writer.name(key); 
					    if(thing!=null && !thing.toString().trim().equals("")){
					    	if(thing instanceof Timestamp){
					    		Date objDate = (Date)thing;
						    	writer.value(CommonUtilities.convertDatetoString(objDate, "HH:mm:ss dd/MM/yyyy"));
					    	}else{
						    	writer.value(thing.toString().replace("'", "\'"));
					    	}    		
					    }else{
					    	writer.value("");
					    }
					    
				  }
				  writer.endObject();
			}			
			
			writer.endArray();
			writer.close();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return outputStream.toString();
	}	
	
}

