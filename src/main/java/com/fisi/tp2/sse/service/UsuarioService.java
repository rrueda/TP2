package com.fisi.tp2.sse.service;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisi.tp2.sse.model.entity.Egresado;

@Transactional(propagation = Propagation.REQUIRED)
public interface UsuarioService {
	
	public Egresado obtenerEgresadoLogin(String codigo, String contrasena) throws Exception;
	public Egresado actualizarEgresadoLogin(Egresado egresado) throws Exception;
	
}
