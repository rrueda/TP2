package com.fisi.tp2.sse.model.bean;

import java.util.List;

public class PreguntaBean{
	
	private Integer preguntaId;
	private Integer tipo;
	private String resultado;
	private String descripcion;
	private List<RespuestaBean> respuestas;
	
	public Integer getPreguntaId() {
		return preguntaId;
	}
	public void setPreguntaId(Integer preguntaId) {
		this.preguntaId = preguntaId;
	}
	public Integer getTipo() {
		return tipo;
	}
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public List<RespuestaBean> getRespuestas() {
		return respuestas;
	}
	public void setRespuestas(List<RespuestaBean> respuestas) {
		this.respuestas = respuestas;
	}
	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}		
}