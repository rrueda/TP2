package com.fisi.tp2.sse.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fisi.tp2.sse.model.bean.MensajeRespuesta;
import com.fisi.tp2.sse.model.bean.PreguntaBean;
import com.fisi.tp2.sse.model.entity.Egresado;
import com.fisi.tp2.sse.model.entity.Encuesta;
import com.fisi.tp2.sse.service.EncuestaService;
import com.google.gson.Gson;

@Controller
public class EncuestaController {

	@Autowired
	EncuestaService encuestaService;

	@RequestMapping(value = "/obtenerPreguntasEncuesta", method = RequestMethod.GET)
	public @ResponseBody String obtenerPreguntasEncuesta(HttpServletRequest request, @RequestParam int idEncuesta) throws Exception {
		String jsonResponse = null;
		Egresado egresado = (Egresado) request.getSession().getAttribute("usuarioSesion");
		String codigo = egresado.getCodigo();
		if (codigo.equals("12200118")) {
			jsonResponse = "";
		}else {
			try {
				System.out.println(idEncuesta);
				jsonResponse = encuestaService.obtenerPreguntasEncuesta(idEncuesta);
			} catch (Exception e) {
				jsonResponse = "";
			}
		}
		System.out.println(
				"Ingresando al controller: 'obtenerPreguntasEncuesta'" + encuestaService.obtenerCantidadEncuestas());

		return jsonResponse;
	}

	@RequestMapping(value = "/guardarCuestionarioResuelto", method = RequestMethod.POST)
	public @ResponseBody MensajeRespuesta guardarCuestionarioResuelto(HttpServletRequest request,
			@RequestBody List<PreguntaBean> preguntas) throws Exception {
		String jsonResponse = null;
		Egresado egresado = (Egresado) request.getSession().getAttribute("usuarioSesion");
		encuestaService.guardarCuestionarioResuelto(preguntas, egresado);
		MensajeRespuesta mensaje = new MensajeRespuesta();
		mensaje.setMessage("Gracias por tomarte un tiempo para respondernos");
		return mensaje;
	}

	@RequestMapping(value = "/obtenerReporte1", method = RequestMethod.GET)
	public @ResponseBody String obtenerReporte1(HttpServletRequest request) throws Exception {
		String jsonResponse = null;
		jsonResponse = encuestaService.obtenerReporte1();
		return jsonResponse;
	}

	@RequestMapping(value = "/obtenerReporteCausaNoTitulo", method = RequestMethod.GET)
	public @ResponseBody String obtenerReporteCausaNoTitulo(HttpServletRequest request) throws Exception {
		String jsonResponse = null;
		jsonResponse = encuestaService.obtenerReporteCausaNoTitulo();
		return jsonResponse;
	}

	@RequestMapping(value = "/obtenerReporteMaximoGrado", method = RequestMethod.GET)
	public @ResponseBody String obtenerReporteMaximoGrado(HttpServletRequest request) throws Exception {
		String jsonResponse = null;
		jsonResponse = encuestaService.obtenerReporteMaximoGrado();
		return jsonResponse;
	}

	@RequestMapping(value = "/obtenerReporteEstimacionExpectativaEscuela", method = RequestMethod.GET)
	public @ResponseBody String obtenerReporteEstimacionExpectativaEscuela(HttpServletRequest request)
			throws Exception {
		String jsonResponse = null;
		jsonResponse = encuestaService.obtenerReporteEstimacionExpectativaEscuela();
		return jsonResponse;
	}

	@RequestMapping(value = "/obtenerReporteRecomiendaFormacionEcuela", method = RequestMethod.GET)
	public @ResponseBody String obtenerReporteRecomiendaFormacionEcuela(HttpServletRequest request) throws Exception {
		String jsonResponse = null;
		jsonResponse = encuestaService.obtenerReporteRecomiendaFormacionEcuela();
		return jsonResponse;
	}

	@RequestMapping(value = "/obtenerReporteNivelRelacionEmpleo", method = RequestMethod.GET)
	public @ResponseBody String obtenerReporteNivelRelacionEmpleo(HttpServletRequest request) throws Exception {
		String jsonResponse = null;
		jsonResponse = encuestaService.obtenerReporteNivelRelacionEmpleo();
		return jsonResponse;
	}

	@RequestMapping(value = "/obtenerReporteNivelSatisfaccionTrabajo", method = RequestMethod.GET)
	public @ResponseBody String obtenerReporteNivelSatisfaccionTrabajo(HttpServletRequest request) throws Exception {
		String jsonResponse = null;
		jsonResponse = encuestaService.obtenerReporteNivelSatisfaccionTrabajo();
		return jsonResponse;
	}

	@RequestMapping(value = "/obtenerReporteNivelRespuestaExigenciaMercado", method = RequestMethod.GET)
	public @ResponseBody String obtenerReporteNivelRespuestaExigenciaMercado(HttpServletRequest request)
			throws Exception {
		String jsonResponse = null;
		jsonResponse = encuestaService.obtenerReporteNivelRespuestaExigenciaMercado();
		return jsonResponse;
	}

	@RequestMapping(value = "/obtenerPuerto", method = RequestMethod.GET)
	public @ResponseBody String obtenerPuerto(HttpServletRequest request) throws Exception {
		Properties propiedades = new Properties();
		InputStream entrada = null;
		String jsonResponse = null;

		try {
			entrada = this.getClass().getClassLoader().getResourceAsStream("configuracion.properties");
			// cargamos el archivo de propiedades
			propiedades.load(entrada);
			// obtenemos las propiedades y las imprimimos
			jsonResponse = propiedades.getProperty("host");
			jsonResponse += ":" + propiedades.getProperty("puerto");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (entrada != null) {
				try {
					entrada.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		Gson gson = new Gson();
		return gson.toJson(jsonResponse);
	}

	@RequestMapping(value = "/guardarEncuesta", method = RequestMethod.POST)
	public @ResponseBody String guardarEncuesta(HttpServletRequest request, @RequestBody Encuesta encuesta)
			throws Exception {
		MensajeRespuesta mensaje = new MensajeRespuesta();
		mensaje.setMessage(encuestaService.guardarEncuesta(encuesta));
		Gson gson = new Gson();
		return gson.toJson(mensaje);
	}
}
