'use strict';

app.controller('reporte1Ctrl', function ($scope, data, causaNoTitulado, maximoGrado) {   
	
	$scope.labels = [];
	$scope.data = [];
	
	for (var int = 0; int < data.length; int++) {
		$scope.labels.push(data[int].titulo);
		$scope.data.push(data[int].cantidad);
	}
	
	$scope.labels1 = [];
	$scope.data1 = [];
	
	for (var int = 0; int < causaNoTitulado.length; int++) {
		$scope.labels1.push(causaNoTitulado[int].causa);
		$scope.data1.push(causaNoTitulado[int].cantidad);
	}
	
	$scope.labels2 = [];
	$scope.data2 = [];
	
	for (var int = 0; int < maximoGrado.length; int++) {
		$scope.labels2.push(maximoGrado[int].grado);
		$scope.data2.push(maximoGrado[int].cantidad);
	}

	if($scope.data.length == 0 && $scope.data1.length == 0 && $scope.data2.length == 0){
		$scope.mensaje = true;
	}
});
