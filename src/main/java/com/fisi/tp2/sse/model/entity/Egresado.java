package com.fisi.tp2.sse.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the egresado database table.
 * 
 */
@Entity


@NamedQueries({
	@NamedQuery(name="Egresado.obtenerEgresadoLogin", query="SELECT e FROM Egresado e WHERE e.codigo = :codigo AND e.contrasena = :contrasena"),
	@NamedQuery(name="Egresado.actualizarEncuesta", query="UPDATE Egresado SET idEncuesta = idEncuesta + 1 WHERE codigo = :codigo")
})
public class Egresado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idEgresado;

	private String apellidos;

	private String celular;

	private String correo;

	private String direccion;

	private int estadoCivil;
	
	private String codigo;
	
	private String contrasena;	

	@Temporal(TemporalType.DATE)
	private Date fechaNacimiento;

	private String nombres;

	private int sexo;

	private String telefono;
	
	private int idEncuesta;

	public int getIdEncuesta() {
		return idEncuesta;
	}

	public void setIdEncuesta(int idEncuesta) {
		this.idEncuesta = idEncuesta;
	}

	//bi-directional many-to-one association to Egresadopregunta
	@OneToMany(mappedBy="egresado")
	private List<Egresadopregunta> egresadopreguntas;

	//bi-directional many-to-one association to Estudio
	@OneToMany(mappedBy="egresado")
	private List<Estudio> estudios;

	//bi-directional many-to-one association to Experiencialaboral
	@OneToMany(mappedBy="egresado")
	private List<Experiencialaboral> experiencialaborals;

	public Egresado() {
	}

	public int getIdEgresado() {
		return this.idEgresado;
	}

	public void setIdEgresado(int idEgresado) {
		this.idEgresado = idEgresado;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCelular() {
		return this.celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public int getEstadoCivil() {
		return this.estadoCivil;
	}

	public void setEstadoCivil(int estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public Date getFechaNacimiento() {
		return this.fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getNombres() {
		return this.nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public int getSexo() {
		return this.sexo;
	}

	public void setSexo(int sexo) {
		this.sexo = sexo;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public List<Egresadopregunta> getEgresadopreguntas() {
		return this.egresadopreguntas;
	}

	public void setEgresadopreguntas(List<Egresadopregunta> egresadopreguntas) {
		this.egresadopreguntas = egresadopreguntas;
	}

	public Egresadopregunta addEgresadopregunta(Egresadopregunta egresadopregunta) {
		getEgresadopreguntas().add(egresadopregunta);
		egresadopregunta.setEgresado(this);

		return egresadopregunta;
	}

	public Egresadopregunta removeEgresadopregunta(Egresadopregunta egresadopregunta) {
		getEgresadopreguntas().remove(egresadopregunta);
		egresadopregunta.setEgresado(null);

		return egresadopregunta;
	}

	public List<Estudio> getEstudios() {
		return this.estudios;
	}

	public void setEstudios(List<Estudio> estudios) {
		this.estudios = estudios;
	}

	public Estudio addEstudio(Estudio estudio) {
		getEstudios().add(estudio);
		estudio.setEgresado(this);

		return estudio;
	}

	public Estudio removeEstudio(Estudio estudio) {
		getEstudios().remove(estudio);
		estudio.setEgresado(null);

		return estudio;
	}

	public List<Experiencialaboral> getExperiencialaborals() {
		return this.experiencialaborals;
	}

	public void setExperiencialaborals(List<Experiencialaboral> experiencialaborals) {
		this.experiencialaborals = experiencialaborals;
	}

	public Experiencialaboral addExperiencialaboral(Experiencialaboral experiencialaboral) {
		getExperiencialaborals().add(experiencialaboral);
		experiencialaboral.setEgresado(this);

		return experiencialaboral;
	}

	public Experiencialaboral removeExperiencialaboral(Experiencialaboral experiencialaboral) {
		getExperiencialaborals().remove(experiencialaboral);
		experiencialaboral.setEgresado(null);

		return experiencialaboral;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	
}