package com.fisi.tp2.sse.model.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the egresadopregunta database table.
 * 
 */
@Entity
@NamedQuery(name="Egresadopregunta.findAll", query="SELECT e FROM Egresadopregunta e")
public class Egresadopregunta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idEgresadoPregunta;

	//bi-directional many-to-one association to Egresado
	@ManyToOne
	@JoinColumn(name="idEgresadoFk")
	private Egresado egresado;

	//bi-directional many-to-one association to Respuesta
	@ManyToOne
	@JoinColumn(name="idRespuestaFk")
	private Respuesta respuesta;

	public Egresadopregunta() {
	}

	public int getIdEgresadoPregunta() {
		return this.idEgresadoPregunta;
	}

	public void setIdEgresadoPregunta(int idEgresadoPregunta) {
		this.idEgresadoPregunta = idEgresadoPregunta;
	}

	public Egresado getEgresado() {
		return this.egresado;
	}

	public void setEgresado(Egresado egresado) {
		this.egresado = egresado;
	}

	public Respuesta getRespuesta() {
		return this.respuesta;
	}

	public void setRespuesta(Respuesta respuesta) {
		this.respuesta = respuesta;
	}

}