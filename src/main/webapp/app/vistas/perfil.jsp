<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>

<title></title>
<link rel="stylesheet" href="app/plugin/bootstrap/bootstrap.min.css">
<link rel="stylesheet"
	href="app/plugin/font-awesome-4.6.3/css/font-awesome.min.css">
<link rel="stylesheet"
	href="app/plugin/sweetalert2/dist/sweetalert2.css" />

</head>
<body>
	<table class="table">
		<tr>
			<th>Nombres</th>
			<td>${usuarioSesion.nombres}</td>
		</tr>
		<tr>
			<th>Apellidos</th>
			<td>${usuarioSesion.apellidos}</td>
		</tr>
		<tr>
			<th>Correo</th>
			<td>${usuarioSesion.correo}</td>
		</tr>
		<tr>
			<th>Dirección</th>
			<td>${usuarioSesion.direccion}</td>
		</tr>
		<tr>
			<th>Celular</th>
			<td>${usuarioSesion.celular}</td>
		</tr>
	</table>

	<button type="button" ng-click="$ctrPerfil.open()">Ver Identificacion Reniec</button>

	<script type="text/ng-template" id="myModalContent1.html">
        <div class="modal-header">
            <h3 class="modal-title" id="modal-title">Cuestionario</h3>
        </div>
        <div class="modal-body" id="modal-body">
            <div style="margin-bottom: 15px">
				<p><b>DNI: &nbsp</b></p>
				<input type="text" id="dni" size="8" for="dni" value="${usuarioSesion.codigo}" disabled>
				<button type="button" ng-click="$ctr1.consultarReniec()">Verificar en Reniec</button>

				<p><b>-Nombre: &nbsp</b></p><input type="text" size="40" for="nombre" value="{{$ctr1.persona.nombre}}" disabled>
				<p><b>-Dirección Actual: &nbsp</b></p><input type="text" size="30" for="direccion" value="{{$ctr1.persona.direccion}}" disabled>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="$ctr1.ok()">Actualizar</button>
            <button class="btn btn-warning" type="button" ng-click="$ctr1.cancel()">Cancelar</button>
        </div>
    </script>

	<script src="app/plugin/jquery/jquery.min.js"></script>
	<script src="app/plugin/angular/angular.min.js"></script>
	<script src="app/plugin/angular/angular-ui-router.js"></script>
	<script src="app/plugin/bootstrap/bootstrap.min.js"></script>
	<script src="app/plugin/ui-bootstrap/ui-bootstrap-tpls-2.2.0.min.js"></script>
	<script src="app/plugin/sweetalert2/dist/sweetalert2.js"></script>
	<script src="app/plugin/chart/Chart.js"></script>
	<script src="app/plugin/angular-chart/angular-chart.js"></script>


	
	<script src="app/scripts/app.js"></script>

	<script src="app/scripts/controlador/inicioCtrl.js"></script>
	<script src="app/scripts/controlador/reporte1Ctrl.js"></script>
	<script src="app/scripts/controlador/reporte2Ctrl.js"></script>
	<script src="app/scripts/controlador/reporte3Ctrl.js"></script>
	<script src="app/scripts/controlador/loginCtrl.js"></script>
	<script src="app/scripts/controlador/perfilCtrl.js"></script>
</body>
</html>