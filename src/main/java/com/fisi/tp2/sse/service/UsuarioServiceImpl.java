package com.fisi.tp2.sse.service;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.fisi.tp2.sse.model.entity.Egresado;
import com.fisi.tp2.sse.persistence.dao.ICrudDAO;
import com.fisi.tp2.sse.utility.NativeQueries;

@Service(value="usuarioService")
public class UsuarioServiceImpl implements UsuarioService {

	@Resource  
	private ICrudDAO<Egresado> egresadoDAO;
	
	public Egresado obtenerEgresadoLogin(String codigo, String contrasena) throws Exception {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("codigo", codigo);
		parametros.put("contrasena", contrasena);
		return egresadoDAO.findObjectByNamedQuery("Egresado.obtenerEgresadoLogin", parametros);
	}
	
	public Egresado actualizarEgresadoLogin(Egresado egresado) throws Exception {
		
		return egresadoDAO.persist(egresado);
	}

}
