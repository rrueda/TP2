'use strict';

app.controller('reporte2Ctrl', function ($scope, data, expectativa) {   
	
	$scope.labels = [];
	$scope.data = [];
	
	for (var int = 0; int < data.length; int++) {
		$scope.labels.push(data[int].recomienda);
		$scope.data.push(data[int].cantidad);
	}
	
	$scope.labels1 = [];
	$scope.data1 = [];
	$scope.series = ["Alumnos"];
	
	for (var int = 0; int < expectativa.length; int++) {
		$scope.labels1.push(expectativa[int].expectativa);
		$scope.data1.push(expectativa[int].cantidad);
	}
	
	if($scope.data.length == 0 && $scope.data1.length == 0){
		$scope.mensaje = true;
	}
});
