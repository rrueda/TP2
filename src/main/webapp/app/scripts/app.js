'use strict';

var app = angular.module('proyecto', ['ui.router', 'ui.bootstrap', 'chart.js'], function($httpProvider) {
});


app.config(function($stateProvider,$urlRouterProvider) {		
	$urlRouterProvider.otherwise('/Perfil');

	$stateProvider
	.state('Inicio', {	
		url: "/Inicio",
		templateUrl : "app/vistas/inicio.html",
		controller:"inicioCtrl",
		resolve : {
			usuario: function ($http,$q,$state){
				var delay = $q.defer();
				var usuario = {};
				usuario.intUsuarioId = 1;
				$http.post('obtenerUsuarioPorPk', usuario)
				.success(function(data) {
					delay.resolve(data);
				}).error(function(data, status, headers, config) {
					$state.go("Error");
				});				
				return delay.promise;
			}
		}				
	})
	.state('Reporte1', {	
		url: "/Reporte1",
		templateUrl : "app/vistas/reporte1.html",
		controller:"reporte1Ctrl",
		resolve : {
			data: function ($http,$q,$state){
				var delay = $q.defer();
				var usuario = {};
				usuario.intUsuarioId = 1;
				$http.get('obtenerReporte1')
				.success(function(data) {
					delay.resolve(data);
				}).error(function(data, status, headers, config) {
					$state.go("Error");
				});				
				return delay.promise;
			},
			causaNoTitulado: function ($http,$q,$state){
				var delay = $q.defer();
				$http.get('obtenerReporteCausaNoTitulo')
				.success(function(data) {
					delay.resolve(data);
				}).error(function(data, status, headers, config) {
					$state.go("Error");
				});				
				return delay.promise;
			},
			maximoGrado: function ($http,$q,$state){
				var delay = $q.defer();
				$http.get('obtenerReporteMaximoGrado')
				.success(function(data) {
					delay.resolve(data);
				}).error(function(data, status, headers, config) {
					$state.go("Error");
				});				
				return delay.promise;
			}
		}
	})
	.state('Reporte2', {	
		url: "/Reporte2",
		templateUrl : "app/vistas/reporte2.html",
		controller:"reporte2Ctrl",
		resolve : {
			data: function ($http,$q,$state){
				var delay = $q.defer();
				$http.get('obtenerReporteRecomiendaFormacionEcuela')
				.success(function(data) {
					delay.resolve(data);
				}).error(function(data, status, headers, config) {
					$state.go("Error");
				});				
				return delay.promise;
			},
			expectativa: function ($http,$q,$state){
				var delay = $q.defer();
				$http.get('obtenerReporteEstimacionExpectativaEscuela')
				.success(function(data) {
					delay.resolve(data);
				}).error(function(data, status, headers, config) {
					$state.go("Error");
				});				
				return delay.promise;
			}
		}
	})
	.state('Reporte3', {	
		url: "/Reporte3",
		templateUrl : "app/vistas/reporte3.html",
		controller:"reporte3Ctrl",
		resolve : {
			data: function ($http,$q,$state){
				var delay = $q.defer();
				$http.get('obtenerReporteNivelRelacionEmpleo')
				.success(function(data) {
					delay.resolve(data);
				}).error(function(data, status, headers, config) {
					$state.go("Error");
				});				
				return delay.promise;
			},
			satisfaccionTrabajo: function ($http,$q,$state){
				var delay = $q.defer();
				$http.get('obtenerReporteNivelSatisfaccionTrabajo')
				.success(function(data) {
					delay.resolve(data);
				}).error(function(data, status, headers, config) {
					$state.go("Error");
				});				
				return delay.promise;
			},
			respuestaMercado: function ($http,$q,$state){
				var delay = $q.defer();
				$http.get('obtenerReporteNivelRespuestaExigenciaMercado')
				.success(function(data) {
					delay.resolve(data);
				}).error(function(data, status, headers, config) {
					$state.go("Error");
				});				
				return delay.promise;
			}
		}
	})
	.state('Encuesta', {	
		url: "/Encuesta",
		templateUrl : "app/vistas/encuesta.html",
		controller:"encuestaCtrl",
		resolve : {
			data: function ($http,$q,$state){
				var delay = $q.defer();
				$http.get('obtenerReporteNivelRelacionEmpleo')
				.success(function(data) {
					delay.resolve(data);
				}).error(function(data, status, headers, config) {
					$state.go("Error");
				});				
				return delay.promise;
			}
		}
	})
	.state('Perfil', {	
		url: "/Perfil",
		templateUrl : "app/vistas/perfil.jsp",
		controller: "perfilCtrl",
		controllerAs: "$ctrPerfil"
	})
	.state('Error',{
		url: "/Error",
		templateUrl : "app/vistas/error.html",
	})									
});
