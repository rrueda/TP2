'use strict';

app.controller('perfilCtrl', function($scope, $http, $state, $uibModal, $q) {
	var $ctrl1 = this;
	$ctrl1.animationsEnabled = true;
	$ctrl1.open = function(size, parentSelector) {
		var parentElem = parentSelector ? angular.element($document[0]
				.querySelector('.modal-demo ' + parentSelector)) : undefined;
		var modalInstance = $uibModal.open({
			animation : $ctrl1.animationsEnabled,
			ariaLabelledBy : 'modal-title',
			ariaDescribedBy : 'modal-body',
			templateUrl : 'myModalContent1.html',
			controller : 'ModalInstanceCtrl1',
			controllerAs : '$ctr1',
			size : size,
			appendTo : parentElem
		});
	};
	
});

app.controller('ModalInstanceCtrl1', function($http, $uibModalInstance) {
	var $ctrl1 = this;
	$ctrl1.puertoWs = function() {
		$http.get('obtenerPuerto').success(function(data) {
			// $uibModalInstance.close($ctrl1.selected.item);
			$ctrl1.puertoWs = data;
		}).error(function(data) {
			console.log("error al obtener puerto");
		});
	};
	
	$ctrl1.puertoWs();
	
	$ctrl1.consultarReniec = function() {
		
		$ctrl1.dni =  angular.element('#dni').val();
		
		$http.get('http://' + $ctrl1.puertoWs + '/Reniec/' + $ctrl1.dni).success(
				function(data) {
					// $uibModalInstance.close($ctrl1.selected.item);
					$ctrl1.persona = data;
				}).error(function(data) {
			console.log("error consumir ws");
		});
	};
	
	$ctrl1.ok = function() {
		$http.put('actualizarDatosEgresado', $ctrl1.persona).success(
				function(data) {
					swal({
						title : 'Enviado',
						type : 'success',
						text : data.message,
						timer : 3000,
						showConfirmButton : false
					});
					$uibModalInstance.close();
				}).error(function(data) {
			console.log("error al guardar datos del egresado");
		});
		window.location.reload(true);
	};
	$ctrl1.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	};
	
});