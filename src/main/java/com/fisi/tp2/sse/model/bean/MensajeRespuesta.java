package com.fisi.tp2.sse.model.bean;

public class MensajeRespuesta{

	String message;	
	Integer valor;
	
	public MensajeRespuesta() {
		// TODO Auto-generated constructor stub
	}
	public MensajeRespuesta(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getValor() {
		return valor;
	}
	public void setValor(Integer valor) {
		this.valor = valor;
	}		
}
