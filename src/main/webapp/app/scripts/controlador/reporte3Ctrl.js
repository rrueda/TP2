'use strict';

app.controller('reporte3Ctrl', function ($scope, data, satisfaccionTrabajo, respuestaMercado) {   
	
	$scope.labels = [];
	$scope.data = [];
	
	for (var int = 0; int < data.length; int++) {
		$scope.labels.push(data[int].porcentaje);
		$scope.data.push(data[int].cantidad);
	}
	
	$scope.labels1 = [];
	$scope.data1 = [];
	
	for (var int = 0; int < satisfaccionTrabajo.length; int++) {
		$scope.labels1.push(satisfaccionTrabajo[int].porcentaje);
		$scope.data1.push(satisfaccionTrabajo[int].cantidad);
	}
	
	$scope.labels2 = [];
	$scope.data2 = [];
	
	for (var int = 0; int < respuestaMercado.length; int++) {
		$scope.labels2.push(respuestaMercado[int].porcentaje);
		$scope.data2.push(respuestaMercado[int].cantidad);
	}
	
	if($scope.data.length == 0 && $scope.data1.length == 0 && $scope.data2.length == 0){
		$scope.mensaje = true;
	}
});
