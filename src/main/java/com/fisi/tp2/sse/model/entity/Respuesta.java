package com.fisi.tp2.sse.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the respuesta database table.
 * 
 */
@Entity
@NamedQuery(name="Respuesta.findAll", query="SELECT r FROM Respuesta r")
public class Respuesta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idRespuesta;

	private String descripcion;

	//bi-directional many-to-one association to Egresadopregunta
	@OneToMany(mappedBy="respuesta")
	private List<Egresadopregunta> egresadopreguntas;

	//bi-directional many-to-one association to Pregunta
	@ManyToOne
	@JoinColumn(name="idPreguntaFk")
	private Pregunta pregunta;

	public Respuesta() {
	}

	public int getIdRespuesta() {
		return this.idRespuesta;
	}

	public void setIdRespuesta(int idRespuesta) {
		this.idRespuesta = idRespuesta;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Egresadopregunta> getEgresadopreguntas() {
		return this.egresadopreguntas;
	}

	public void setEgresadopreguntas(List<Egresadopregunta> egresadopreguntas) {
		this.egresadopreguntas = egresadopreguntas;
	}

	public Egresadopregunta addEgresadopregunta(Egresadopregunta egresadopregunta) {
		getEgresadopreguntas().add(egresadopregunta);
		egresadopregunta.setRespuesta(this);

		return egresadopregunta;
	}

	public Egresadopregunta removeEgresadopregunta(Egresadopregunta egresadopregunta) {
		getEgresadopreguntas().remove(egresadopregunta);
		egresadopregunta.setRespuesta(null);

		return egresadopregunta;
	}

	public Pregunta getPregunta() {
		return this.pregunta;
	}

	public void setPregunta(Pregunta pregunta) {
		this.pregunta = pregunta;
	}

}