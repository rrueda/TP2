package com.fisi.tp2.sse.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the encuesta database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Encuesta.findAll", query="SELECT e FROM Encuesta e"),
	@NamedQuery(name="Encuesta.obtenerEncuesta", query="SELECT e FROM Encuesta e WHERE e.idencuesta = :idEncuesta")
})

public class Encuesta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idencuesta;

	private String descripcion;

	private boolean estado;

	@Temporal(TemporalType.DATE)
	private Date fechaFin;

	@Temporal(TemporalType.DATE)
	private Date fechaInicio;

	private String nombre;

	//bi-directional many-to-one association to Administrador
	@ManyToOne
	@JoinColumn(name="idAdministradorFk")
	private Administrador administrador;

	//bi-directional many-to-one association to Pregunta
	@OneToMany(mappedBy="encuesta")
	private List<Pregunta> preguntas;

	public Encuesta() {
	}

	public int getIdencuesta() {
		return this.idencuesta;
	}

	public void setIdencuesta(int idencuesta) {
		this.idencuesta = idencuesta;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean getEstado() {
		return this.estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public Date getFechaFin() {
		return this.fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Date getFechaInicio() {
		return this.fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Administrador getAdministrador() {
		return this.administrador;
	}

	public void setAdministrador(Administrador administrador) {
		this.administrador = administrador;
	}

	public List<Pregunta> getPreguntas() {
		return this.preguntas;
	}

	public void setPreguntas(List<Pregunta> preguntas) {
		this.preguntas = preguntas;
	}

	public Pregunta addPregunta(Pregunta pregunta) {
		getPreguntas().add(pregunta);
		pregunta.setEncuesta(this);

		return pregunta;
	}

	public Pregunta removePregunta(Pregunta pregunta) {
		getPreguntas().remove(pregunta);
		pregunta.setEncuesta(null);

		return pregunta;
	}

}