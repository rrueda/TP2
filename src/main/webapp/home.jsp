<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html ng-app="proyecto">
<head>
<title>SSE</title>
<link rel="stylesheet" href="app/plugin/bootstrap/bootstrap.min.css">
<link rel="stylesheet"
	href="app/plugin/font-awesome-4.6.3/css/font-awesome.min.css">
<link rel="stylesheet"
	href="app/plugin/sweetalert2/dist/sweetalert2.css" />
</head>
<body ng-controller="homeCtrl as $ctrl">
	<form id="formLogout" method="get" action="logout">
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="/Inicio">SSE</a>
				</div>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#">${usuarioSesion.nombres}</a></li>
					<li><input type="hidden" id="idEncuesta" value="${usuarioSesion.idEncuesta}"></li>
					<li><input type="hidden" id="idPersona" value="${usuarioSesion.codigo}"></li>
					<li><a href="#"
						onclick="document.getElementById('formLogout').submit()"><i
							class="fa fa-sign-out" aria-hidden="true"></i> Cerrar sesión</a></li>
				</ul>
			</div>
		</nav>
	</form>
	<div class="row">
		<div class="col-sm-3">
			<ul id="nav-tabs-wrapper"
				class="nav nav-tabs nav-pills nav-stacked well">
				<li class="active item"><a href="#/Perfil">Información
						personal</a></li>
				<li class="item"><a href="#/Reporte1"
					
					ng-hide="${usuarioSesion.codigo!='12200118'}">Reporte Situación
						académica</a></li>
				<li class="item"><a href="#/Reporte2"
					ng-hide="${usuarioSesion.codigo!='12200118'}">Reporte Formación
						académica</a></li>
				<li class="item"><a href="#/Reporte3"
					ng-hide="${usuarioSesion.codigo!='12200118'}">Reporte Desempeño
						profesional</a></li>
				<li class="item"><a href="#/Encuesta"
					ng-hide="${usuarioSesion.codigo!='12200118'}">Crear encuesta</a></li>
			</ul>
		</div>
		<div class="col-sm-9">
			<div ui-view class="tab-content"></div>
		</div>
	</div>

 	<script type="text/ng-template" id="myModalContent.html">
        <div class="modal-header">
            <h3 class="modal-title" id="modal-title">Cuestionario</h3>
        </div>
        <div class="modal-body" id="modal-body">
            <div style="margin-bottom: 15px" ng-repeat="item in $ctrl.items">
                <p><b>{{item.descripcion}}</b></p>
				<p ng-if="item.tipo == 8" ng-repeat="respuesta in item.respuestas" for="{{respuesta.descripcion}}">					
					<input id="{{respuesta.descripcion}}" type="radio" ng-model="$ctrl.items[$parent.$parent.$index].resultado" ng-value="respuesta.respuestaId" name="rpta{{$parent.$parent.$index}}">
					{{respuesta.descripcion}}
				</p>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="$ctrl.ok()">Enviar</button>
            <button class="btn btn-warning" type="button" ng-click="$ctrl.cancel()">Cancelar</button>
        </div>
    </script> 

	<script src="app/plugin/jquery/jquery.min.js"></script>
	<script src="app/plugin/angular/angular.min.js"></script>
	<script src="app/plugin/angular/angular-ui-router.js"></script>
	<script src="app/plugin/bootstrap/bootstrap.min.js"></script>
	<script src="app/plugin/ui-bootstrap/ui-bootstrap-tpls-2.2.0.min.js"></script>
	<script src="app/plugin/sweetalert2/dist/sweetalert2.js"></script>
	<script src="app/plugin/chart/Chart.js"></script>
	<script src="app/plugin/angular-chart/angular-chart.js"></script>

	<script type="text/javascript">
		$(".item").click(function() {
			$(".item").removeClass("active");
			$(this).addClass("active");
		});
	</script>

	<script src="app/scripts/app.js"></script>
	<script src="app/scripts/controlador/homeCtrl.js"></script>
	<script src="app/scripts/controlador/inicioCtrl.js"></script>
	<script src="app/scripts/controlador/reporte1Ctrl.js"></script>
	<script src="app/scripts/controlador/reporte2Ctrl.js"></script>
	<script src="app/scripts/controlador/reporte3Ctrl.js"></script>
	<script src="app/scripts/controlador/encuestaCtrl.js"></script>
	<script src="app/scripts/controlador/loginCtrl.js"></script>
	<script src="app/scripts/controlador/perfilCtrl.js"></script>
</body>
</html>