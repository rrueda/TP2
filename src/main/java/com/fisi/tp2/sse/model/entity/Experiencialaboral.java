package com.fisi.tp2.sse.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the experiencialaboral database table.
 * 
 */
@Entity
@NamedQuery(name="Experiencialaboral.findAll", query="SELECT e FROM Experiencialaboral e")
public class Experiencialaboral implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idExperienciaLaboral;

	private String cargo;

	private String descripcion;

	private String empresa;

	@Temporal(TemporalType.DATE)
	private Date fechaFin;

	@Temporal(TemporalType.DATE)
	private Date fechaInicio;

	private String rubro;

	//bi-directional many-to-one association to Egresado
	@ManyToOne
	@JoinColumn(name="idEgresadoFk")
	private Egresado egresado;

	public Experiencialaboral() {
	}

	public int getIdExperienciaLaboral() {
		return this.idExperienciaLaboral;
	}

	public void setIdExperienciaLaboral(int idExperienciaLaboral) {
		this.idExperienciaLaboral = idExperienciaLaboral;
	}

	public String getCargo() {
		return this.cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEmpresa() {
		return this.empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public Date getFechaFin() {
		return this.fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Date getFechaInicio() {
		return this.fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getRubro() {
		return this.rubro;
	}

	public void setRubro(String rubro) {
		this.rubro = rubro;
	}

	public Egresado getEgresado() {
		return this.egresado;
	}

	public void setEgresado(Egresado egresado) {
		this.egresado = egresado;
	}

}