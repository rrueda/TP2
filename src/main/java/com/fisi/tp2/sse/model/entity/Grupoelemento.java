package com.fisi.tp2.sse.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the grupoelemento database table.
 * 
 */
@Entity
@NamedQuery(name="Grupoelemento.findAll", query="SELECT g FROM Grupoelemento g")
public class Grupoelemento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idGrupoElemento;

	private String nombre;

	//bi-directional many-to-one association to Elemento
	@OneToMany(mappedBy="grupoelemento")
	private List<Elemento> elementos;

	public Grupoelemento() {
	}

	public int getIdGrupoElemento() {
		return this.idGrupoElemento;
	}

	public void setIdGrupoElemento(int idGrupoElemento) {
		this.idGrupoElemento = idGrupoElemento;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Elemento> getElementos() {
		return this.elementos;
	}

	public void setElementos(List<Elemento> elementos) {
		this.elementos = elementos;
	}

	public Elemento addElemento(Elemento elemento) {
		getElementos().add(elemento);
		elemento.setGrupoelemento(this);

		return elemento;
	}

	public Elemento removeElemento(Elemento elemento) {
		getElementos().remove(elemento);
		elemento.setGrupoelemento(null);

		return elemento;
	}

}