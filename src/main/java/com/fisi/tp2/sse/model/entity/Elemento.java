package com.fisi.tp2.sse.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the elemento database table.
 * 
 */
@Entity
@NamedQuery(name="Elemento.findAll", query="SELECT e FROM Elemento e")
public class Elemento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idElemento;

	private String nombre;

	private int valor;

	//bi-directional many-to-one association to Elemento
	@ManyToOne
	@JoinColumn(name="idElementoFk")
	private Elemento elemento;

	//bi-directional many-to-one association to Elemento
	@OneToMany(mappedBy="elemento")
	private List<Elemento> elementos;

	//bi-directional many-to-one association to Grupoelemento
	@ManyToOne
	@JoinColumn(name="idGrupoElementoFk")
	private Grupoelemento grupoelemento;

	public Elemento() {
	}

	public int getIdElemento() {
		return this.idElemento;
	}

	public void setIdElemento(int idElemento) {
		this.idElemento = idElemento;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getValor() {
		return this.valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public Elemento getElemento() {
		return this.elemento;
	}

	public void setElemento(Elemento elemento) {
		this.elemento = elemento;
	}

	public List<Elemento> getElementos() {
		return this.elementos;
	}

	public void setElementos(List<Elemento> elementos) {
		this.elementos = elementos;
	}

	public Elemento addElemento(Elemento elemento) {
		getElementos().add(elemento);
		elemento.setElemento(this);

		return elemento;
	}

	public Elemento removeElemento(Elemento elemento) {
		getElementos().remove(elemento);
		elemento.setElemento(null);

		return elemento;
	}

	public Grupoelemento getGrupoelemento() {
		return this.grupoelemento;
	}

	public void setGrupoelemento(Grupoelemento grupoelemento) {
		this.grupoelemento = grupoelemento;
	}

}