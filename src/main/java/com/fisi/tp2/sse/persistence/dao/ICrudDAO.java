package com.fisi.tp2.sse.persistence.dao;

import java.util.List;
import java.util.Map;

public interface ICrudDAO<T> {

	public T persist(T t);
	
	public T persistList(List<T> t) throws Exception ;
	
	public T findById(Class<T> type, Object id);
	
	public void delete(T t) throws Exception;
	
	public List<T> findByNamedQuery(String namedQuery, Map<String, Object> parameters);
	
	public T findObjectByNamedQuery(String namedQuery, Map<String, Object> parameters);

	public List findByNamedQueryTransformer(Class<?> type, String namedQuery, Map<String, Object> parameters);
	
	public Object findObjectByNamedQueryTransformer(Class<?> type, String namedQuery, Map<String, Object> parameters);
	
	public String findByNamedQueryJson(String namedQuery, Map<String, Object> parameters);

	public int executeUpdateNamedQuery(String namedQuery,Map<String, Object> parameters);
			
	public String executeNativeQueryJson(String strQuery, Map<String, Object> parameters);
	
	public List findByNativeQueryTransformer(Class<?> type, String nativeQuery, Map<String, Object> parameters);
	
}