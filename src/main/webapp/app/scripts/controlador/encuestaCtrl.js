'use strict';

app.controller('encuestaCtrl', function ($scope, $state, $http) {   
	
	$scope.encuesta = {};
	$scope.encuesta.preguntas = [];
	$scope.preguntaActual = {};
	$scope.preguntaActual.respuestas = [];
	$scope.preguntaActual.tipo = "8";
	$scope.respuestaActual = {};
	$scope.respuestaActual.descripcion = "";
	$scope.tipos = {"8": "Respuesta única", "7": "Selección múltiple" , "9": "Respuesta abierta"};
	$scope.flag = false;
	$scope.flag1 = false;
	$scope.respuestaAbierta = false;
	$scope.respuestaActual = "";
	
	$scope.agregarRespuesta = function(){	
		if($scope.respuestaActual == ""){
			$scope.respuestaActual = "-";
		}
		$scope.preguntaActual.respuestas.push(angular.copy($scope.respuestaActual));
		$scope.respuestaActual = {};
		$scope.respuestaActual.descripcion = "";
		$scope.flag = true;		
	}

	$scope.agregarPregunta = function(){
		if($scope.flag){			
			$scope.encuesta.preguntas.push(angular.copy($scope.preguntaActual));
			$scope.preguntaActual = {};
			$scope.preguntaActual.respuestas = [];
			$scope.preguntaActual.tipo = "8";
			$scope.flag1 = true;
			$scope.flag = false;
			$('#myModal').modal('toggle');
		}
	}
	
	$scope.guardarEncuesta = function(){
		if($scope.flag1 && $scope.encuesta.nombre != undefined){
			console.log($scope.encuesta);
			$http.post('guardarEncuesta', $scope.encuesta)
			.success(function(data, status, headers, config) {
				swal({
					  title: 'Enviado',
					  type: 'success',
					  text: data.message,
					  timer: 3000,
					  showConfirmButton: false
				});	
			}).error(function(data, status, headers, config) {
				$state.go("Error");
			});	
		}
	}
	
	$scope.trueRespuestaAbierta = function(){
		$scope.respuestaAbierta = true;
	}
	
	$scope.falseRespuestaAbierta = function(){
		$scope.respuestaAbierta = false;
	}
});
