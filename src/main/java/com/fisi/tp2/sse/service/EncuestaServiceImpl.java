package com.fisi.tp2.sse.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.fisi.tp2.sse.model.bean.PreguntaBean;
import com.fisi.tp2.sse.model.bean.RespuestaBean;
import com.fisi.tp2.sse.model.entity.Administrador;
import com.fisi.tp2.sse.model.entity.Egresado;
import com.fisi.tp2.sse.model.entity.Egresadopregunta;
import com.fisi.tp2.sse.model.entity.Encuesta;
import com.fisi.tp2.sse.model.entity.Pregunta;
import com.fisi.tp2.sse.model.entity.Respuesta;
import com.fisi.tp2.sse.persistence.dao.ICrudDAO;
import com.fisi.tp2.sse.utility.NativeQueries;
import com.google.gson.Gson;

@Service(value="encuestaService")
public class EncuestaServiceImpl implements EncuestaService {

	@Resource  
	private ICrudDAO<Pregunta> preguntaDAO;
	
	@Resource  
	private ICrudDAO<Egresadopregunta> egresadoPreguntaDAO;
	
	@Resource  
	private ICrudDAO<Respuesta> respuestaDAO;
	
	@Resource  
	private ICrudDAO<Encuesta> encuestaDAO;
	
	@Resource  
	private ICrudDAO<Egresado> egresadoDAO;

	public String obtenerPreguntasEncuesta(int idEncuesta) throws Exception {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("idEncuesta", idEncuesta);
		Encuesta encuesta = encuestaDAO.findObjectByNamedQuery("Encuesta.obtenerEncuesta", param);
		System.out.println(encuesta.getDescripcion());
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("encuesta", encuesta.getIdencuesta());
		List<Pregunta> lista = preguntaDAO.findByNamedQuery("Pregunta.obtenerPreguntasEncuesta", parametros);
		List<PreguntaBean> preguntas = new ArrayList<>();
		PreguntaBean pregunta = new PreguntaBean();
		RespuestaBean respuesta = new RespuestaBean();
		for (int i = 0; i < lista.size(); i++) {
			pregunta = new PreguntaBean();
			pregunta.setPreguntaId(lista.get(i).getIdPregunta());
			pregunta.setTipo(lista.get(i).getTipo());
			pregunta.setDescripcion(lista.get(i).getDescripcion());
			pregunta.setRespuestas(new ArrayList<RespuestaBean>());;
			for (int j = 0; j < lista.get(i).getRespuestas().size(); j++) {
				respuesta = new RespuestaBean();
				respuesta.setRespuestaId(lista.get(i).getRespuestas().get(j).getIdRespuesta());
				respuesta.setDescripcion(lista.get(i).getRespuestas().get(j).getDescripcion());
				pregunta.getRespuestas().add(respuesta);
			}
			preguntas.add(pregunta);
		}
		Gson gson = new Gson();		
		return gson.toJson(preguntas);
	}

	@Override
	public Integer guardarCuestionarioResuelto(List<PreguntaBean> preguntas, Egresado egresado) throws Exception {
		Respuesta rpta = new Respuesta();
		Egresadopregunta ep = new Egresadopregunta();
		for (PreguntaBean pregunta : preguntas) {
			ep = new Egresadopregunta();
			if(pregunta.getResultado() != null && pregunta.getResultado() != ""){
				rpta = new Respuesta();
				if(pregunta.getTipo() == 9){					
					Pregunta preg = new Pregunta();
					preg.setIdPregunta(pregunta.getPreguntaId());	
										
					rpta.setPregunta(preg);
					rpta.setDescripcion(pregunta.getResultado());
					rpta = respuestaDAO.persist(rpta);
				}else if(pregunta.getTipo() == 8){
					rpta.setIdRespuesta(Integer.parseInt(pregunta.getResultado()));					
				}
				ep.setEgresado(egresado);
				ep.setRespuesta(rpta);
				egresadoPreguntaDAO.persist(ep);
			}
		}
		
		Map<String, Object> parametros = new HashMap<String, Object>();
		
		parametros.put("codigo", egresado.getCodigo());
		
		egresadoDAO.executeUpdateNamedQuery("Egresado.actualizarEncuesta", parametros);
		
		return 1;
	}
	
	@Override
	public String obtenerReporte1() throws Exception {
		return encuestaDAO.executeNativeQueryJson(NativeQueries.reporte1, null);
	}

	@Override
	public String obtenerReporteCausaNoTitulo() throws Exception {
		// TODO Auto-generated method stub
		return encuestaDAO.executeNativeQueryJson(NativeQueries.reporteCausaNoTitulo, null);
	}
	
	@Override
	public String obtenerReporteMaximoGrado() throws Exception {
		return encuestaDAO.executeNativeQueryJson(NativeQueries.reporteMaximoGrado, null);
	}
	
	@Override
	public String obtenerReporteEstimacionExpectativaEscuela() throws Exception {
		return encuestaDAO.executeNativeQueryJson(NativeQueries.reporteEstimacionExpectativaEscuela, null);
	}

	@Override
	public String obtenerReporteRecomiendaFormacionEcuela() throws Exception {
		return encuestaDAO.executeNativeQueryJson(NativeQueries.reporteRecomiendaFormacionEcuela, null);
	}

	@Override
	public String obtenerReporteNivelRelacionEmpleo() throws Exception {
		return encuestaDAO.executeNativeQueryJson(NativeQueries.reporteNivelRelacionEmpleo, null);
	}
	
	@Override
	public String obtenerReporteNivelSatisfaccionTrabajo() throws Exception {
		return encuestaDAO.executeNativeQueryJson(NativeQueries.reporteNivelSatisfaccionTrabajo, null);
	}

	@Override
	public String obtenerReporteNivelRespuestaExigenciaMercado() throws Exception {
		return encuestaDAO.executeNativeQueryJson(NativeQueries.reporteNivelRespuestaExigenciaMercado, null);
	}

	@Override
	public String guardarEncuesta(Encuesta encuesta) throws Exception {
		encuesta.setEstado(false);
		Administrador admin = new Administrador();
		admin.setIdAdministrador(1);
		encuesta.setAdministrador(admin);
		encuesta = encuestaDAO.persist(encuesta);
		
		for (Pregunta p : encuesta.getPreguntas()) {
			p.setEncuesta(encuesta);
			p = preguntaDAO.persist(p);
			for (Respuesta r : p.getRespuestas()) {
				r.setPregunta(p);
				respuestaDAO.persist(r);
			}
		}
		return "La encuesta ha sido generada correctamente";
	}
	
	@Override
	public int obtenerCantidadEncuestas() throws Exception{
		
		String json = encuestaDAO.executeNativeQueryJson(NativeQueries.obtenerCantidadEncuestas, null);
		Gson gson = new Gson();
		Properties[] properties = gson.fromJson(json, Properties[].class);
		return Integer.parseInt(properties[0].getProperty("count(*)"));
	}
}
